import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './not-found/not-found.component';
import { PrimeNgGridRoutes } from '../prime-ng-grid/prime-ng-grid.routing.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/primeNgGrid',
    pathMatch: 'full'
  },
  {
    path: '',
    children: PrimeNgGridRoutes
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    NotFoundComponent
  ]
})
export class RoutingModule { }
