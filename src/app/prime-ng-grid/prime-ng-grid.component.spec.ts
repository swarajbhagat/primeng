import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgGridComponent } from './prime-ng-grid.component';

describe('PrimeNgGridComponent', () => {
  let component: PrimeNgGridComponent;
  let fixture: ComponentFixture<PrimeNgGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
