import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';

import { PrimeNgGridComponent } from './prime-ng-grid.component';

export const PrimeNgGridRoutes: Routes = [
  { path: 'primeNgGrid', component: PrimeNgGridComponent}
];

class PrimeNgGridRoutingModule { }