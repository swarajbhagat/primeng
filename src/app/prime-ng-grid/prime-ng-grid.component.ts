import { Component, OnInit } from '@angular/core';

import { ProjectTracker } from './project-tracker';
import { GridDataService } from './grid-data.service';

@Component({
  selector: 'app-prime-ng-grid',
  templateUrl: './prime-ng-grid.component.html',
  styleUrls: ['./prime-ng-grid.component.scss']
})
export class PrimeNgGridComponent implements OnInit {

  projectTracker: ProjectTracker[];
  cols: any[];
  frozenCols: any[];
  frozenWidth: number;
  certificationStages = [{ label: 'Registration', value: 'Registration' },
  { label: 'Preliminary', value: 'Preliminary' },
  { label: 'Final', value: 'Final' }];

  constructor(private gridDataService: GridDataService) { }

  ngOnInit() {
    this.gridDataService.getProjectTrackerData().subscribe((pt: ProjectTracker[]) => {
      this.projectTracker = pt;
    });

    this.cols = [
      
      { field: 'registrationDate', header: 'Registration Date', width: 100 },
      { field: 'preliminaryCertificationDate', header: 'Preliminary Certification Date', width: 100 },
      { field: 'certificationStage', header: 'Certification Stage', width: 100 },
      { field: 'finalCertificationDate', header: 'Final Certification Date', width: 100 }
    ];

    this.frozenCols = [
      { field: 'projectName', header: 'Project Name', width: 200 },
      { field: 'subprojectName', header: 'Subproject Name', width: 200 }
    ];

    this.frozenWidth = 400;
  }

}
