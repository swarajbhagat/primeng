import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ProjectTracker } from './project-tracker';

@Injectable({
  providedIn: 'root'
})
export class GridDataService {

  constructor() { }

  projectTrackerData = [
    {
      projectName: 'Hiranandani', subprojectName: 'Bhoomi', certificationStage: 'Final',
      registrationDate: '2018/10/18', preliminaryCertificationDate: '2018/11/01', finalCertificationDate: '2018/11/18'
    },
    {
      projectName: 'Gajraj', subprojectName: 'Bhoomi1', certificationStage: 'Preliminary',
      registrationDate: '2018/10/19', preliminaryCertificationDate: '2018/11/02', finalCertificationDate: '2018/11/20'
    },
    {
      projectName: 'Lodha', subprojectName: 'Bhoomi2', certificationStage: 'Registration',
      registrationDate: '2018/10/20', preliminaryCertificationDate: '2018/11/03', finalCertificationDate: '2018/11/23'
    },
    {
      projectName: 'Hooda', subprojectName: 'Bhoomi3', certificationStage: 'Registration',
      registrationDate: '2018/10/21', preliminaryCertificationDate: '2018/11/04', finalCertificationDate: '2018/11/23'
    },
    {
      projectName: 'Hiranandani', subprojectName: 'Bhoomi4', certificationStage: 'Final',
      registrationDate: '2018/10/10', preliminaryCertificationDate: '2018/11/05', finalCertificationDate: '2018/11/25'
    }
  ];

  getProjectTrackerData() {
    return new Observable((observer) => {
      observer.next(this.projectTrackerData);
      observer.complete();
    });
  }
}
