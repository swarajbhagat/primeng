
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';

import { PrimeNgGridComponent } from './prime-ng-grid.component';

@NgModule({
  declarations: [
    PrimeNgGridComponent
  ],
  imports: [
    CommonModule,
    TableModule,
    MultiSelectModule,
    InputTextModule,
    DropdownModule
  ]
})
export class PrimeNgGridModule { }
