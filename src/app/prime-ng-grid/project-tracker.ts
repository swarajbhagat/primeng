export interface ProjectTracker {
    projectName,
    subprojectName,
    certificationStage,
    registrationDate,
    preliminaryCertificationDate,
    finalCertificationDate
}
