import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RoutingModule } from './routing/routing.module';
import { AppComponent } from './app.component';
import { PrimeNgGridModule } from './prime-ng-grid/prime-ng-grid.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    PrimeNgGridModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
